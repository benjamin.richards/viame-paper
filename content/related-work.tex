\section{Related Work}
\label{sec:related-work}

There are currently many open-source computer vision packages available on the internet. Many of the most popular repositories, such as OpenCV~\cite{opencv}, Caffe~\cite{caffe}, and Theano~\cite{theano}, typically contain lower-level functions for image processing operations but lack full end to end processing pipelines that combine multiple stages of processing in a streaming architecture. Our pipeline framework also allows input and output endcaps to be easily reconfigured; for example, core algorithmic pipelines can get input data from a UDP streaming source instead of a video or image list reader via a simple config file change. Pipeline architectures and system settings can be changed easily in the same config file with recompilation. Most current computer vision toolkits also lack standard input and output formats for storing output products, such as object detections, which are contained within our framework. These standard outputs are designed to work with the included GUI and scoring tools.

At the other end of the spectrum, there are several open-source media streaming frameworks such as GStreamer \cite{gstreamer} and FFmpeg \cite{ffmpeg}. These libraries were designed, however, primarily for multimedia applications and not explicitly for image and video processing. One of the key elements of our pipeline framework is an individual process class definition, which contains definable subroutines for loading model files at the beginning of processing, in addition to defining what actions to perform when new data is received (typically a new image in a video sequence, or a new metadata packet). These useful elements do not exist in GStreamer and FFmpeg. The platform also contains common data types for passing between process nodes, and base classes to simplify defining specific types of algorithm processes, e.g. separate base classes for object detectors and stereo correspondence algorithms. Finally, our framework allows pipelines to be defined using a directed acyclic graph (DAG), which is more general than the simple linear pipelines of GStreamer and FFmpeg. The platform manages the complexity of inter-process synchronization, communications and dependencies, which can be tricky and problematic to program correctly in other frameworks.

\begin{figure}[b!]
  \centering
  \includegraphics[width=\linewidth]{content/figures/pipeline_ex.png}
  \caption{Simple pipeline abstraction containing an image reader, an arbitrary object detector, an output display, and an output writer process.}
  \label{fig:pipeline}
\end{figure}

\begin{figure*}[t!]
  \centering
  \includegraphics[width=0.6\linewidth]{content/figures/process_def.png}
  \caption{An overview of an individual process. All items, such as the number of ports, types of ports, main step function, and green auxiliary functions are optionally defined by process implementers. The algorithm itself can either live within the process, or be a wrapper around an external repository or binary.}
  \label{fig:process}
\end{figure*}

In the marine science environment, computer vision has yet to reach its full potential. There is some work in techniques for automated detection, identification, measurement, tracking, and counting fish in underwater optical data streams \cite{chuang2013multiple,chuang2014recognizing,chuang2014supervised,chuang2016feature,gu2016automatic,salman2016fish,shafait2016fish}. However, few of these systems are fully automated, with all of the functions required to produce highly successful and accurate results \cite{shortis2013review}. Consequently there is little operational use of automated analysis. Furthermore, researchers and algorithms tend to remain specialized in a specific type of fish and/or collection system, whereas there may be significant gain in efficiently experimenting across domains. The proposed framework facilitates such collaborations and experiments by making it easy and cheap to share algorithms, implementations, and data.
